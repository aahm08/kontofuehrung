#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>

FILE * kontospeicher;

void einzahlen(float &kontostand, float einzahlen) {
	printf("Betrag der eingezahlt werden soll: ");
	scanf("%f", &einzahlen);
	kontostand = kontostand + einzahlen;
	printf("\n Kontostand: %.2f", kontostand);
}

void auszahlen(float &kontostand, float auszahlen) {
	
	printf("Betrag der ausgezahlt werden soll: ");
	scanf("%f", &auszahlen);

	//keine negativen kontost�nde
	if (kontostand-auszahlen <= 0) {
		printf("\n Kein Auszahlen moeglich, da Kontostand zu gering ist.");
		printf("\n Kontostand: %.2f \n", kontostand);
	}
	else {
		kontostand = kontostand - auszahlen;
		printf("\n Kontostand: %.2f \n", kontostand);
	}
}

int main() {
	float kontostand = 0, betrag = 0;
	int x = 0;

	//derzeitiger kontostand wird aufgerufen
	kontospeicher = fopen("kontostand.txt", "r");
	fscanf(kontospeicher, "%f", &kontostand);
	fclose(kontospeicher);

	//Dauerschleife bis 4 (exit) gew�hlt wird
	for (int i = 1; i <10; i--) {
		printf("Waehlen sie aus was sie machen wollen: \n 1 = Einzahlen \n 2 = Auszahlen \n 3 = Kontostand abrufen \n 4 = Abbruch \n");
		
		scanf("%d", &x);


		if (x == 1) {
			einzahlen(kontostand, betrag);
		}
		else if (x == 2) {
			auszahlen(kontostand, betrag);
		}
		else if (x == 3) {
			printf("kontostand: %.2f \n", kontostand);
		}
		else if (x == 4) {
			printf("Programm wird abgebrochen.");

			//speichert kontostand in txt
			kontospeicher = fopen("kontostand.txt", "w");
			fprintf(kontospeicher, "%f", kontostand);

			//beendet das programm
			exit(0);
		}
		else {
			printf("\n Fehlerhafte Eingabe!");
		}
		printf("\n\n\n");
	}

	getchar();
	getchar();
	getchar();

	return 0;
}