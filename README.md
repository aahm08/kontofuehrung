Name: Kontosimulation

Inhalt: Ein- und ausazhlen soll möglich sein. 
        Speichern der gesamten Daten. Infos über den Kontostand/Kontoauszug über die letzten Transaktionen.
        
Größte Herausforderung: Speichern/erhaltung der Daten. Benutzerdaten speichern. 

Lösungsansatz: Erstellung einer Variablen Struktur für Datenspeicherung der Nutzer. Speicherung der Daten auf dem Computer als Dateien (zb. als text Datei).
               Zugriff und Änderung von Daten über die Dateien.