Fortschritt: 
05.01.16 - Programm fertigestellt.

    Kontostand wird aus Datei gelesen.
    Menü mit mehreren Optionen wird angezeigt.
    Zur Auswahl steht: Einzahlen, Auszahlen, Kontostand abrufen & Programm beenden.
    Die vohrigen 2 Punkte laufen in einer Dauerschleife bis "Abbruch" ausgewählt wird.
    Bei Auszahlen wird eingegebener Betrag von dem jetzigen Kontostand abgezogen, falls möglich.
    Bei Einzahlen wird eingegebener Betrag dem Kontostand hinzugefügt.
    Bei Kontostand abrufen wird der aktuelle Kontostand ausgegeben.
    Bei Abbruch wird per exit funktion das programm beendet & voher noch der kontostand gespeichert.
    Falls Eingabe fehlerhaft ist gibt das programm das an & man kann erneut auswählen.
